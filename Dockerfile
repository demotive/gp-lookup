FROM ruby:2.2.3

# Set the locale
ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8

ENV ALLOWED_ORIGINS=http://localhost:4567

WORKDIR /usr/src/app

EXPOSE 4567

RUN apt-get update && apt-get install -y --force-yes nodejs --no-install-recommends && rm -rf /var/lib/apt/lists/*

COPY Gemfile Gemfile.lock ./

RUN bundle install

COPY . .

CMD ["bundle", "exec", "rackup", "--host", "0.0.0.0", "-p", "4567"]